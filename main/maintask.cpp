#include "maintask.h"

MainTask::MainTask() : display(&memory) {
}

void MainTask::start() {
    xTaskCreate(run, "mainTask", 1024 * 8, this, configMAX_PRIORITIES, NULL);
}

void MainTask::printTimes() {
    printf("single: %.1fs\tdouble: %.1fs\ttriple: %.1fs\n", memory.times[0] / 1000.0, memory.times[1] / 1000.0, memory.times[2] / 1000.0);
}

void MainTask::setNewState(FsmState newState) {
    state = newState;
    stateMS = 0;
    printf("entering state %i\n", newState);

    switch(newState) {
        case STATE_IDLE:
            memory.autoDose = -1;
            printf("\nready for some more coffee.\n");
            selectMode(memory.mode, false);
            printTimes();
            break;

        case STATE_MANUAL:
            memory.autoDose = MODE_MANUAL;
            printf("manual mode selected.\n");
            break;

        case STATE_AUTO:
            memory.autoDose = MODE_SINGLE;
            printf("automatic mode selected. dispensing for a single...\n");
            break;

        default:
            break;
    }
}

void MainTask::incrementDoseCount() {
    memory.totalDoses++;
    writeValues();
}

void MainTask::readValues() {
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &my_handle);
    ESP_ERROR_CHECK(err);

    if(err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        err = nvs_get_u32(my_handle, "time1", &memory.times[0]);
        err = nvs_get_u32(my_handle, "time2", &memory.times[1]);
        err = nvs_get_u32(my_handle, "time3", &memory.times[2]);
        err = nvs_get_u32(my_handle, "mode", &memory.mode);
        err = nvs_get_u32(my_handle, "totalDoses", &memory.totalDoses);

        switch(err) {
            case ESP_OK:
                printTimes();
                break;

            case ESP_ERR_NVS_NOT_FOUND:
                puts("The value is not initialized yet!");
                break;

            default :
                printf("Error (%s) reading!\n", esp_err_to_name(err));
        }

        // Close
        nvs_close(my_handle);
    }

    selectMode(memory.mode, false);
}

void MainTask::writeValues() {
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open("storage", NVS_READWRITE, &my_handle);
    ESP_ERROR_CHECK(err);

    if(err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        puts("storing values to flash...");
        err = nvs_set_u32(my_handle, "time1", memory.times[0]);
        err = nvs_set_u32(my_handle, "time2", memory.times[1]);
        err = nvs_set_u32(my_handle, "time3", memory.times[2]);
        err = nvs_set_u32(my_handle, "mode", memory.mode);
        err = nvs_set_u32(my_handle, "totalDoses", memory.totalDoses);
        nvs_close(my_handle);
    }
}

void MainTask::stepFSM(bool pressed, bool edge) {
    switch(state) {
        case STATE_IDLE:

            // wait for some action
            if(edge && pressed) {
                setNewState(STATE_PRESSED);
                printf("let's go!\n");
                motorOn = true;
            }

            break;

        case STATE_PRESSED:

            // turn motor on, not yet clear in what mode we will run
            if(stateMS >= MS_HOLD_MANUAL) {
                setNewState(STATE_MANUAL); // manual mode
            } else if(!pressed) {
                setNewState(STATE_AUTO);
            }

            break;

        case STATE_MANUAL:
            // dispense until released
            memory.timeMs = stateMS + MS_HOLD_MANUAL;
            printf("manual time: %.1fs\n", memory.timeMs / 1000.0);

            if(!pressed) {
                motorOn = false;
                setNewState(STATE_WAIT_IDLE);
                printf("manual mode finished\n");
            }

            break;

        case STATE_AUTO: {
            // normal auto mode: two taps within MS_MIN_AUTO give a double. time can be aborted with a further press
            int time = memory.times[memory.autoDose];
            memory.timeMs = time - stateMS;
            printf("time: %i, stateMs: %i\n", time, stateMS);
            printf("time remaining for a [%i] is %.1fs\n", memory.autoDose, memory.timeMs / 1000.0);

            if(stateMS <= MS_MIN_AUTO) {
                if(edge && !pressed) {
                    if(memory.autoDose < 3) { // aaargh
                        // we need a bigger
                        memory.autoDose++;
                        printf("...gimme bigger dose\n");
                    } else {
                        // we are just pressing wildly, abort...
                        motorOn = false;
                        printf("...abort due to wild pressing\n");
                        setNewState(STATE_IDLE);
                    }
                }
            } else if(stateMS >= time) {
                // auto mode elapsed
                printf("...auto time exceeded\n");
                motorOn = false;
                setNewState(STATE_WAIT_IDLE);
            } else if(pressed) {
                // auto mode abort before elapsed
                motorOn = false;
                printf("...auto time stopped manually after %.1fs\n", stateMS / 1000.0);
                setNewState(STATE_IDLE);
            }
        }
        break;

        case STATE_WAIT_IDLE:

            // we have the chance to add some more at the end
            if(edge) {
                if(pressed) {
                    printf("...gimme some more\n");
                    motorOn = true;
                } else {
                    printf("...enough?\n");
                    motorOn = false;
                    setNewState(STATE_WAIT_IDLE);
                }
            }

            if(pressed) {
                printf("plus time %.1fs\n", stateMS / 1000.0);
                memory.timeMs += TASK_MS;
            } else if(stateMS > MS_WAIT_AFTER) {
                // we do not need more, go back to idle
                printf("enough for now.\n");
                incrementDoseCount();
                setNewState(STATE_IDLE);
            }

            break;
    }

    stateMS += TASK_MS;
}
void MainTask::initialize() {
    gpio_config_t io_conf;
    // init button 1
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = 1ULL << GPIO_BUTTON1_IN;
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    gpio_config(&io_conf);
    // init button 2
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = 1ULL << GPIO_BUTTON2_IN;
    io_conf.pull_down_en = GPIO_PULLDOWN_ENABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    gpio_config(&io_conf);
    // button input
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.pin_bit_mask = 1ULL << GPIO_BUTTON;
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pull_down_en = GPIO_PULLDOWN_ENABLE;
    gpio_config(&io_conf);
    // motor output
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = 1ULL << GPIO_MOTOR;
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    gpio_config(&io_conf);
}

void MainTask::run(void *arg) {
    MainTask *task = reinterpret_cast<MainTask*>(arg);
    task->initialize();
    task->readValues();
    task->display.initialize();
    bool pressed = false;
    bool button1Pressed = false;
    bool button2Pressed = false;
    bool bothPressed = false;
    // Initialize NVS
    esp_err_t err = nvs_flash_init();

    if(err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }

    task->readValues();
    // first boot
    task->setNewState(STATE_IDLE);
    bool pressed_old = pressed;
    bool button1PressedOld = button1Pressed;
    bool button2PressedOld = button2Pressed;
    bool bothPressedOld = bothPressed;
    bool ignoreRelease = false;
    int buttonHoldTime = 0;

    while(1) {
        pressed = gpio_get_level(task->GPIO_BUTTON) == false;
        button1Pressed = gpio_get_level(task->GPIO_BUTTON1_IN) == false;
        button2Pressed = gpio_get_level(task->GPIO_BUTTON2_IN) == false;
        bool bothPressed = button1Pressed && button2Pressed;
        bool edge = false;

        if(pressed != pressed_old) {
            pressed_old = pressed;
            edge = true;
        }

        if(task->state == STATE_IDLE) { // only allow mode switch if STATE_IDLE
            bool changeMode = false;
            int newMode = task->memory.mode;

            if(button1Pressed != button2Pressed) {
                buttonHoldTime += TASK_MS;
            } else {
                buttonHoldTime = 0;
            }

            if(button1Pressed != button1PressedOld) {
                printf("button 1 %s\n", button1Pressed ? "pressed" : "released");

                if(!ignoreRelease && !button1Pressed) {
                    newMode = task->memory.mode - 1;

                    if(newMode < 0) {
                        newMode = MODE_AUTOMATIC;
                    }

                    changeMode = true;
                }
            }

            if(button2Pressed != button2PressedOld) {
                printf("button 2 %s\n", button2Pressed ? "pressed" : "released");

                if(!ignoreRelease && !button2Pressed) {
                    newMode = task->memory.mode + 1;

                    if(newMode > MODE_AUTOMATIC) {
                        newMode = 0;
                    }

                    changeMode = true;
                }
            }

            bool pressAndHold = false;

            // programming time on release edge or long press
            if(!ignoreRelease && !bothPressed && task->memory.programming) {
                pressAndHold = (button2Pressed && buttonHoldTime >= 200);

                if((button2PressedOld && !button2Pressed) || pressAndHold) {
                    if(task->memory.times[task->memory.mode] < 99000) {
                        task->memory.times[task->memory.mode] += task->getDoseStepMS(pressAndHold, buttonHoldTime);
                    }
                }

                pressAndHold = (button1Pressed && buttonHoldTime > 500);

                if((button1PressedOld && !button1Pressed) || pressAndHold) {
                    if(task->memory.times[task->memory.mode] > 100) {
                        task->memory.times[task->memory.mode] -= task->getDoseStepMS(pressAndHold, buttonHoldTime);
                    }
                }
            } else if(changeMode) {
                task->selectMode(newMode);
            }

            if(bothPressed != bothPressedOld) {
                if(bothPressed) {
                    ignoreRelease = true;
                    printf("both buttons pressed\n");

                    // auto mode has no programming
                    if(task->memory.mode != MODE_AUTOMATIC) {
                        task->memory.programming = !task->memory.programming;
                    }

                    if(!task->memory.programming) {
                        // save
                        // round to 100ms steps
                        task->memory.times[task->memory.mode] = (task->memory.times[task->memory.mode] / 100) * 100;
                        task->writeValues();
                        task->selectMode(task->memory.mode, false);
                    }
                }
            }

            button1PressedOld = button1Pressed;
            button2PressedOld = button2Pressed;
            bothPressedOld = bothPressed;
        }

        //printf("ignore release %i\n", ignoreRelease);

        if(task->memory.mode == MODE_AUTOMATIC) {
            task->stepFSM(pressed, edge);
        } else {
            task->simpleDoseFsm(pressed, edge);
        }

        // clear ignore as soon both are released
        ignoreRelease &= !(!button1Pressed && !button2Pressed);
        gpio_set_level(task->GPIO_MOTOR, task->motorOn);
        vTaskDelay(pdMS_TO_TICKS(TASK_MS));
    }
}

int MainTask::getDoseStepMS(bool pressAndHold, int pressAndHoldTime) {
    // these values are very intuitive for me ;)
    if(pressAndHold) {
        if(pressAndHoldTime >= 2000) {
            return 50;
        } else {
            return 30;
        }
    } else {
        return 100;
    }
}

void MainTask::selectMode(int newMode, bool save) {
    memory.mode = newMode;

    if(newMode != MODE_AUTOMATIC) { // no time for auto
        memory.timeMs = memory.times[newMode];
    } else {
        memory.timeMs = 0;
    }

    printf("mode now is %i with time %i\n", newMode, memory.timeMs);

    if(save) {
        writeValues();
    }
}
void MainTask::simpleDoseFsm(bool pressed, bool edge) {
    bool stateSwitch = pressed && edge;

    switch(state) {
        case STATE_IDLE:
            if(stateSwitch) {
                setNewState(STATE_AUTO);
                memory.autoDose = memory.mode;
                printf("starting dose %i\n", memory.mode);
                motorOn = true;
                stateMS = 0;
            }

            break;

        case STATE_AUTO: {
            int time = memory.times[memory.mode];
            memory.timeMs = time - stateMS;
            printf("time: %i, stateMs: %i\n", time, stateMS);
            printf("time remaining for a [%i] is %.1fs\n", memory.mode, memory.timeMs / 1000.0);

            if(stateSwitch || stateMS >= time) {
                motorOn = false;
                setNewState(STATE_WAIT_IDLE);
            }
        }
        break;

        case STATE_WAIT_IDLE:
            if(stateMS > MS_WAIT_AFTER) {
                // we do not need more, go back to idle
                printf("enough for now.\n");
                incrementDoseCount();
                setNewState(STATE_IDLE);
                break;
            }

        default:
            break;
    }

    stateMS += TASK_MS;
}
