#ifndef MAINTASK_H
#define MAINTASK_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "hal/gpio_types.h"
#include "nvs_flash/include/nvs_flash.h"
#include "nvs_flash/include/nvs.h"
#include "driver/gpio.h"

#include "memory.h"
#include "display.h"
#include "esp_log.h"



#define TASK_MS 50          // 20Hz I/O polling, times have 0.1s resolution
#define MS_HOLD_MANUAL 500  // select manual mode when press and hold longer than 0.5s
#define MS_WAIT_AFTER 1000  // after dispensing complete, can subsequently add some more manually
#define MS_MIN_AUTO 1750    // we have this time to increase the doses in AUTO mode

class MainTask {
public:
    MainTask();
    void start();

private:
    Memory memory;
    Display display;
    static void run(void *arg);
    void writeValues();
    void readValues();

    bool requestSaveParameters = false;

    enum FsmState {
        STATE_IDLE,
        STATE_PRESSED,
        STATE_MANUAL,
        STATE_AUTO,
        STATE_WAIT_IDLE
    } state = STATE_IDLE;

    bool motorOn = false;
    int stateMS = 0;
    void printTimes();
    void setNewState(FsmState newState);
    void stepFSM(bool pressed, bool edge);

    const gpio_num_t GPIO_BUTTON1_IN = GPIO_NUM_0;      // button 1
    const gpio_num_t GPIO_BUTTON2_IN = GPIO_NUM_35;     // button 2

    // changed
    const gpio_num_t GPIO_MOTOR = GPIO_NUM_27;      // motor solid state relay output
    const gpio_num_t GPIO_BUTTON = GPIO_NUM_21;     // button input

    void initialize();
    void simpleDoseFsm(bool pressed, bool edge);
    void selectMode(int newMode, bool save = true);
    void incrementDoseCount();
    int getDoseStepMS(bool pressAndHold, int pressAndHoldTime);
};

#endif // MAINTASK_H
