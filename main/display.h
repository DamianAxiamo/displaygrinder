#ifndef DISPLAY_H
#define DISPLAY_H

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "memory.h"

extern "C" {
#include "tft.h"
#include "spiffs_vfs.h"
}

// ==========================================================
// Define which spi bus to use TFT_VSPI_HOST or TFT_HSPI_HOST
#define SPI_BUS TFT_HSPI_HOST
// ==========================================================

class Display {
public:
    Display(Memory *mem);
    void initialize();
private:
    void disp_header(char *info);
    bool spiffsMounted = false;
    Memory *memory;
    static void run(void *arg);
    char text[20];
};

#endif // DISPLAY_H
