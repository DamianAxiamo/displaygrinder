/* silvoa PID by damian.weber@axiamo.com
 * CC0 license
*/
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "esp_log.h"
#include "driver/uart.h"
#include "esp_sleep.h"

static const int RX_BUF_SIZE = 100;
#define RXD_PIN (GPIO_NUM_4)
#define TXD_PIN (GPIO_NUM_5)

enum Symbol {
    SYM_ZERO,
    SYM_ONE,
    SYM_SYNC,
    SYM_ERROR
};

void init(void) {
    const uart_config_t uart_config = {
        .baud_rate = 92000,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_REF_TICK,
    };
    uart_driver_install(UART_NUM_1, RX_BUF_SIZE * 2, 0, 0, NULL, 0);
    uart_param_config(UART_NUM_1, &uart_config);
    uart_set_pin(UART_NUM_1, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
}

enum Symbol parseSymbol(uint8_t data) {
    switch(data) {
    case 0b00000000:
    case 0b10000000:
    case 0b11000000:
        return SYM_ZERO;
    case 0b11100000:
    case 0b11110000:
    case 0b11111000:
        return SYM_SYNC;
    case 0b11111100:
    case 0b11111110:
    case 0b11111111:
        return SYM_ONE;
    default:
        ESP_LOGE("Parse", "Invalid symbol: 0x%2x", data);
        return SYM_ERROR;
    }
}

float toCelsius(uint16_t data) {
    return (data * 200) / 2048.0 - 50;
}

bool parseFrame(uint8_t *data, uint8_t *result) {
    uint8_t value = 0;
    int ones = 0;
    // check frame consistency. first byte must be sync
    if(parseSymbol(data[0]) == SYM_SYNC) {
        // parse all 8 data bits
        for(int i = 1; i < 9; i++) {
            value <<= 1;
            if(parseSymbol(data[i]) == SYM_ONE) {
                value |= 1;
                ones++;
            }
        }
        // parse parity bit
        if(parseSymbol(data[9]) == SYM_ONE) {
            ones++;
        }
        // check parity
        if(ones & 0x01) {
            ESP_LOGE("Parse", "parity error!");
            esp_log_buffer_hex("Data", data, 10);
            return false;
        } else {
            //esp_log_buffer_hex("Data", data, 10);
        }
    } else {
        ESP_LOGE("Parse", "out of sync!");
        return false;
    }
    *result = value;
    return true;
}

static void rx_task(void *arg) {
    static const char *RX_TASK_TAG = "RX_TASK";
    esp_log_level_set(RX_TASK_TAG, ESP_LOG_INFO);
    uint8_t* data = (uint8_t*) malloc(RX_BUF_SIZE+1);

    // synchronize to byte stream
    bool isSynced = false;
    int parseFails = 0;

    while (1) {
        if(!isSynced) {
            bool gotBytes = false;
            int pauseCount = 2;
            for(int i = 0; i < 10; i++) {
                const int rxBytes = uart_read_bytes(UART_NUM_1, data, 20, 20 / portTICK_RATE_MS);
                if(!gotBytes) {
                    gotBytes = rxBytes == 20;
                } else if(rxBytes == 0) {
                    if(--pauseCount == 0) {
                        i = 10;
                    }
                }
            }
            if(pauseCount > 0) {
                ESP_LOGE("SYNC", "failed");
                vTaskDelay(500 / portTICK_RATE_MS);
            } else {
                isSynced = true;
                ESP_LOGI("SYNC", "successful");
            }
        } else {
            const int rxBytes = uart_read_bytes(UART_NUM_1, data, 20, 300 / portTICK_RATE_MS);
            bool success = false;
            if(rxBytes == 20) {
                uint16_t word = 0;
                uint8_t byte = 0;
                float degrees = 0;
                // high byte
                if(parseFrame(data, &byte)) {
                    word = byte;
                    word <<= 8;
                    // low byte
                    if(parseFrame(&data[10], &byte)) {
                        word |= byte;
                        degrees = toCelsius(word);
                        success = true;
                        parseFails = 0;
                        ESP_LOGI("Parse", "Temp: %.1f °C", degrees);
                    }
                }
            } else {
                ESP_LOGW("UART", "burst not 20 bytes! got %i", rxBytes);
            }
            parseFails += success ? 0 : 1;
            if(parseFails >= 5) {
                isSynced = false;
            }
        }
    }
}

void app_main(void) {

    init();
    xTaskCreate(rx_task, "uart_rx_task", 1024*4, NULL, configMAX_PRIORITIES, NULL);
    while(1) {
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}
