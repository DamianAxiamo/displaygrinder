#ifndef MEMORY_H
#define MEMORY_H

#include <stdio.h>

enum ActiveMode : int {
    MODE_SINGLE = 0,
    MODE_DOUBLE,
    MODE_TRIPLE,
    MODE_AUTOMATIC,
    MODE_MANUAL,
    MODE_MAX
};

struct Memory {
    uint32_t times[3] = { 5000, 8000, 12000};  // default values

    int autoDose;   // 0, 1, 2 in auto mode
    uint32_t mode;  // MainTask::ActiveMode > times[mode] is active time
    bool programming = false;

    uint32_t totalDoses = 0; // total sum of dispensed doses
    uint32_t timeMs = 0; // running time, doses count down, manual counts up
};

#endif // MEMORY_H
