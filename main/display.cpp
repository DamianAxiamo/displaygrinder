#include "display.h"

Display::Display(Memory *mem) : memory(mem) {
}
static const char *file_fonts[3] = {"/spiffs/fonts/DotMatrix_M.fon", "/spiffs/fonts/Ubuntu.fon", "/spiffs/fonts/Grotesk24x48.fon"};

void Display::initialize() {
    esp_err_t ret;
    // === SET GLOBAL VARIABLES ==========================
    // ===================================================
    // ==== Set maximum spi clock for display read    ====
    //      operations, function 'find_rd_speed()'    ====
    //      can be used after display initialization  ====
    tft_max_rdclock = 8000000;
    // ===================================================
    // ====================================================================
    // === Pins MUST be initialized before SPI interface initialization ===
    // ====================================================================
    TFT_PinsInit();
    // ====  CONFIGURE SPI DEVICES(s)  ====================================================================================
    spi_lobo_device_handle_t spi;
    spi_lobo_bus_config_t buscfg = {
        .mosi_io_num = PIN_NUM_MOSI,            // set SPI MOSI pin
        .miso_io_num = PIN_NUM_MISO,            // set SPI MISO pin
        .sclk_io_num = PIN_NUM_CLK,             // set SPI CLK pin
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = 6 * 1024,
    };
    spi_lobo_device_interface_config_t devcfg = {
        .mode = 0,                              // SPI mode 0
        .clock_speed_hz = 8000000,              // Initial clock out at 8 MHz
        .spics_io_num = -1,                     // we will use external CS pin
        .spics_ext_io_num = PIN_NUM_CS,         // external CS pin
        .flags = LB_SPI_DEVICE_HALFDUPLEX,      // ALWAYS SET  to HALF DUPLEX MODE!! for display spi
    };
    // ====================================================================================================================
    vTaskDelay(500 / portTICK_RATE_MS);
    printf("\r\n==============================\r\n");
    printf("TFT display DEMO, LoBo 11/2017\r\n");
    printf("==============================\r\n");
    printf("Pins used: miso=%d, mosi=%d, sck=%d, cs=%d\r\n", PIN_NUM_MISO, PIN_NUM_MOSI, PIN_NUM_CLK, PIN_NUM_CS);
    printf("==============================\r\n\r\n");
    // ==================================================================
    // ==== Initialize the SPI bus and attach the LCD to the SPI bus ====
    ret = spi_lobo_bus_add_device(SPI_BUS, &buscfg, &devcfg, &spi);
    assert(ret == ESP_OK);
    printf("SPI: display device added to spi bus (%d)\r\n", SPI_BUS);
    tft_disp_spi = spi;
    // ==== Test select/deselect ====
    ret = spi_lobo_device_select(spi, 1);
    assert(ret == ESP_OK);
    ret = spi_lobo_device_deselect(spi);
    assert(ret == ESP_OK);
    printf("SPI: attached display device, speed=%u\r\n", spi_lobo_get_speed(spi));
    printf("SPI: bus uses native pins: %s\r\n", spi_lobo_uses_native_pins(spi) ? "true" : "false");
    // ================================
    // ==== Initialize the Display ====
    printf("SPI: display init...\r\n");
    TFT_display_init();
#ifdef TFT_START_COLORS_INVERTED
    TFT_invertDisplay(1);
#endif
    printf("OK\r\n");
#if USE_TOUCH == TOUCH_TYPE_STMPE610
    stmpe610_Init();
    vTaskDelay(10 / portTICK_RATE_MS);
    uint32_t tver = stmpe610_getID();
    printf("STMPE touch initialized, ver: %04x - %02x\r\n", tver >> 8, tver & 0xFF);
#endif
    // ---- Detect maximum read speed ----
    tft_max_rdclock = find_rd_speed();
    printf("SPI: Max rd speed = %u\r\n", tft_max_rdclock);
    // ==== Set SPI clock used for display operations ====
    spi_lobo_set_speed(spi, DEFAULT_SPI_CLOCK);
    printf("SPI: Changed speed to %u\r\n", spi_lobo_get_speed(spi));
    tft_font_rotate = 0;
    tft_text_wrap = 0;
    tft_font_transparent = 0;
    tft_font_forceFixed = 0;
    tft_gray_scale = 0;
    TFT_setGammaCurve(DEFAULT_GAMMA_CURVE);
    TFT_setRotation(LANDSCAPE_FLIP);
    TFT_resetclipwin();
    spiffsMounted = vfs_spiffs_register();
    //
    tft_fg = TFT_WHITE;
    tft_bg = TFT_BLACK;
    TFT_jpg_image(CENTER, CENTER, 0, SPIFFS_BASE_PATH"/images/test4.jpg", NULL, 0);
    tft_fg = TFT_MAGENTA;
    // show dosecounter
    TFT_setFont(DEFAULT_FONT, NULL);
    sprintf(text, "%i", memory->totalDoses);
    TFT_print(text, 10, 110);
    // show credits
    TFT_setFont(USER_FONT, file_fonts[0]); // dotmatrix font
    sprintf(text, "dsr");

    for(int i = 0; i < 100; i++) {
        TFT_print(text, 190, i);
        vTaskDelay(1);
    }

    vTaskDelay(50);
    xTaskCreate(run, "displayTask", 1024 * 8, this, configMAX_PRIORITIES, NULL);
}


void Display::run(void *arg) {
    Display *task = reinterpret_cast<Display*>(arg);
    TFT_fillScreen(tft_bg);
    tft_fg = TFT_CYAN;
    TFT_resetclipwin();
    TFT_setFont(USER_FONT, file_fonts[0]); // dotmatrix font
    int fontHeight = TFT_getfontheight();
    int topMargin = 0;
    int spacing = 10;
    int displayMode = -100;
    uint8_t y1 = topMargin;
    uint8_t y2 = topMargin + (spacing + fontHeight);
    uint8_t y3 = 130;
    int xMargin = 25;
    char text1[20];
    char text[20];
    const char modesChars[4] = {'1', '2', '3', 'A'};
    int blinkCount = 0;
    const int loopMS = 50; // 20Hz
    TickType_t t;
    // [1]  [2]  [3]  [A]
    bool showingBar = false;
    bool isProgramming = false;
    bool forceUpdate = false;
    bool hidePhaseOld = false;
    bool hidePhase = false;
    const int blinkCountPeriod = 8;
    // manually position 7 segment digits
    //                       1   0   .   6    for 10.6
    int charPositions[4] = { 0, 60, 110, 160 };
    int currentDose = -1;
    int currentDoseOld = -1;

    while(1) {
        currentDose = task->memory->autoDose;
        t = xTaskGetTickCount();

        // on enter and exit programming, we force update
        if(isProgramming != task->memory->programming) {
            isProgramming = task->memory->programming;
            forceUpdate = true;
            blinkCount = 0;
        }

        if(++blinkCount > blinkCountPeriod) {
            blinkCount = 0;
        }

        // blink selected mode while programming and while count > half of phase time
        hidePhase = (blinkCount > (blinkCountPeriod / 2)) & isProgramming;

        if(hidePhaseOld != hidePhase) {
            forceUpdate = true;
            hidePhaseOld = hidePhase;
        }

        if(displayMode != task->memory->mode) {
            forceUpdate = true;
            displayMode = task->memory->mode;
        }

        if(forceUpdate) {
            int index = 0;
            memset(text, ' ', sizeof(text));

            for(int i = 0; i < 4; i++) {
                text[index + 0] = !hidePhase && (i == task->memory->mode) ? '[' : ' ' ;
                text[index + 1] = modesChars[i];
                text[index + 2] = !hidePhase && (i == task->memory->mode) ? ']' : ' ';
                index += 3;
            }

            text[sizeof(text) - 1] = '\0';
            tft_fg = TFT_MAGENTA;
            TFT_setFont(USER_FONT, file_fonts[0]); // dotmatrix font
            TFT_print(text, 25, y1);
            forceUpdate = false;
        }

        // populate time to draw to 7 segment display
        if(task->memory->programming) {
            sprintf(text, "%04.1f s", task->memory->times[task->memory->mode] / 1000.0);
        } else {
            if(task->memory->mode == MODE_AUTOMATIC && task->memory->autoDose == -1) {
                sprintf(text, "%04.1f", 0.0);
            } else {
                sprintf(text, "%04.1f", task->memory->timeMs / 1000.0);
            }
        }

        // draw 7 segment time if changed
        if(strcmp(text, text1) != 0) {
            tft_fg = TFT_CYAN;
            TFT_setFont(FONT_7SEG, NULL);

            // print chars that changed
            for(int i = 0; i < 4; i++) {
                if(text[i] != text1[i]) {
                    char c[2];

                    // a space will not clear previous character so we need to draw over to hide it
                    if(i == 0 && text1[0] != '0' && text[0] == '0') {
                        TFT_fillRect(0, y2, 60, TFT_getfontheight(), tft_bg);
                    } else {
                        sprintf(c, "%c", text[i]);
                        TFT_print(c, charPositions[i], y2);
                    }
                }
            }

            memcpy(text1, text, 20);
        }

        // bottom progress bar
        if(task->memory->autoDose != MODE_MANUAL && currentDose != -1) {
            if(!showingBar) {
                sprintf(text, "\r");
                TFT_print(text, xMargin, y3);
                TFT_fillRect(0, y3, 240, 5, TFT_CYAN);
            }

            showingBar = true;
            int doseTime = task->memory->times[currentDose];
            printf("dosetime is %i\n", doseTime);

            if(task->memory->timeMs > 0) {
                int elapsed = (doseTime - task->memory->timeMs);
                printf("doseTime: %i timeMS: %i\n", doseTime, task->memory->timeMs);
                int pixels = ((doseTime - task->memory->timeMs) * 240) / doseTime + 5;
                TFT_fillRect(0, y3, pixels, 5, TFT_MAGENTA);
            }
        } else if(showingBar) {
            // clear bar
            showingBar = false;
            TFT_fillRect(0, y3, 240, 5, tft_bg);
        }

        if(currentDoseOld != currentDose) {
            currentDoseOld = currentDose;
            showingBar = false;
        }

        vTaskDelayUntil(&t, pdMS_TO_TICKS(loopMS));
    }
}
